import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'reto-front-end';

  constructor() {
    
  }
  ngOnInit(): void {
    AOS.init({
      // Global settings:
      // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
      offset: 120, // offset (in px) from the original trigger point
      duration: 1000, // values from 0 to 3000, with step 50ms
      easing: 'ease', // default easing for AOS animations
      mirror: true, // whether elements should animate out while scrolling past them
      anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
    
    });
    this.noreferrer();
  }

  noreferrer(){
    document.addEventListener("DOMContentLoaded", function() {
    var links = document.querySelectorAll("a");
    var numLinks = links.length;
    for (var i = 0; i < numLinks; i++) {
      var link = links[i];
      link.setAttribute("target", "_blank");
      link.setAttribute("rel", "noopener noreferrer");
    }
    });
  }

  ngAfterViewInit() {
    
  }
}
