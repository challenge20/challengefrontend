import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderCustomersComponent } from './slider-customers.component';

describe('SliderCustomersComponent', () => {
  let component: SliderCustomersComponent;
  let fixture: ComponentFixture<SliderCustomersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SliderCustomersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
