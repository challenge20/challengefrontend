import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/interfaces/customer';
// import Swiper core and required modules
import SwiperCore, { Autoplay, Pagination, Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Autoplay, Pagination, Navigation]);

@Component({
  selector: 'app-slider-customers',
  templateUrl: './slider-customers.component.html'
})
export class SliderCustomersComponent implements OnInit {

  customers: Customer[] = [
    {nomCustomer: 'Goldman Sachs logo', urlCustomer: 'https://about.gitlab.com/customers/goldman-sachs/', dirImg: '../../../assets/img/customers/logo_goldman_sachs.svg'},
    {nomCustomer: 'Siemens logo', urlCustomer: 'https://about.gitlab.com/customers/siemens/', dirImg: '../../../assets/img/customers/logo_siemens.bw.svg'},
    {nomCustomer: 'NVIDIA logo', urlCustomer: 'https://about.gitlab.com/customers/Nvidia/', dirImg: '../../../assets/img/customers/logo_nvidia.svg'},
    {nomCustomer: 'ESA logo', urlCustomer: 'https://about.gitlab.com/customers/european-space-agency/', dirImg: '../../../assets/img/customers/esa_logo_gray.svg'},
    {nomCustomer: 'Cloud Native Computing Foundation logo', urlCustomer: 'https://about.gitlab.com/customers/cncf/', dirImg: '../../../assets/img/customers/logo_cncf-color.svg'},
    {nomCustomer: 'Ticketmaster logo', urlCustomer: 'https://about.gitlab.com/blog/2017/06/07/continuous-integration-ticketmaster/', dirImg: '../../../assets/img/customers/logo_ticketmaster.svg'},
  ]

  responsive: any ={
    '360': {
      slidesPerView: 2,
      spaceBetween: 20
    },
    '640': {
      slidesPerView: 3,
      spaceBetween: 20
    },
    '768': {
      slidesPerView: 4,
      spaceBetween: 40
    },
    '1024': {
      slidesPerView: 6,
      spaceBetween: 50
    }
  }

  autoplay = {
    delay: 1000
  }
  
  constructor() { 

  }

  ngOnInit(): void {

  }
  //*ngFor="let customer of customers"

}
