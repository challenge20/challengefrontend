import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio.component';

const routes: Routes = [
  { path: '', component: InicioComponent, 
    children: [      
      { path: 'inicio', component: InicioComponent },
      { path: '' ,  redirectTo: '/inicio', pathMatch: 'full'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InicioRoutingModule { }
