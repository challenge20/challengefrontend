import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InicioRoutingModule } from './inicio-routing.module';
import { InicioComponent } from './inicio.component';
import { SliderCustomersComponent } from '../../components/slider-customers/slider-customers.component';
import { SwiperModule } from "swiper/angular";
import { SectionHeroComponent } from './section-hero/section-hero.component';
import { SectionCostumersComponent } from './section-costumers/section-costumers.component';
import { SectionFeatureComponent } from './section-feature/section-feature.component';
import { SectionTestimonialComponent } from './section-testimonial/section-testimonial.component';
import { SectionTrialComponent } from './section-trial/section-trial.component';
import { SectionPriceComponent } from './section-price/section-price.component';
import { SectionBeneficeComponent } from './section-benefice/section-benefice.component';


@NgModule({
  declarations: [
    InicioComponent,
    SliderCustomersComponent,
    SectionHeroComponent,
    SectionCostumersComponent,
    SectionFeatureComponent,
    SectionTestimonialComponent,
    SectionTrialComponent,
    SectionPriceComponent,
    SectionBeneficeComponent,
  ],
  imports: [
    CommonModule,
    InicioRoutingModule,
    SwiperModule
  ]
})
export class InicioModule { }
