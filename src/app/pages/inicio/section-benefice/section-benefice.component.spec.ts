import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionBeneficeComponent } from './section-benefice.component';

describe('SectionBeneficeComponent', () => {
  let component: SectionBeneficeComponent;
  let fixture: ComponentFixture<SectionBeneficeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionBeneficeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionBeneficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
