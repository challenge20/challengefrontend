import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionCostumersComponent } from './section-costumers.component';

describe('SectionCostumersComponent', () => {
  let component: SectionCostumersComponent;
  let fixture: ComponentFixture<SectionCostumersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionCostumersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionCostumersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
