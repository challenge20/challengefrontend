import { Component, OnInit } from '@angular/core';
import SwiperCore, { EffectFade, Navigation, Pagination,SwiperOptions } from "swiper";

SwiperCore.use([EffectFade, Navigation, Pagination]);

@Component({
  selector: 'app-section-testimonial',
  templateUrl: './section-testimonial.component.html',
  styleUrls: ['./section-testimonial.component.sass']
})
export class SectionTestimonialComponent implements OnInit {

  config: SwiperOptions = {
    simulateTouch:false,
    allowTouchMove: false,
    autoplay: true,
    loop: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: false
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  }

  constructor() { }

  ngOnInit(): void {

  }

  
}
