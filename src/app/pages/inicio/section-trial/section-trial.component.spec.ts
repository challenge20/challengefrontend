import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionTrialComponent } from './section-trial.component';

describe('SectionTrialComponent', () => {
  let component: SectionTrialComponent;
  let fixture: ComponentFixture<SectionTrialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionTrialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
